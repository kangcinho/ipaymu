import React from 'react'
import Layout from './components/UI/Layout/Layout'
import Team from './pages/team/Team'

const App = () => {
  return (
    <React.Fragment>
      <Layout>
        <Team/>
      </Layout>
    </React.Fragment>
  )
}

export default App