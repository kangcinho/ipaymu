import React from 'react'

const Button = (props) => {
    return (
        <div onClick={() => {props.setCurrentPage(props.no)}} className={`${props.no === props.currentPage ? "bg-blue-700": "bg-blue-500"} text-white p-1 rounded-full w-8 h-8 hover:bg-blue-700 hover:cursor-pointer active:bg-blue-500`}>
            <div className="flex justify-center items-center">
                <span>{props.no}</span>
            </div>
        </div>
    )
}

const PaginationButton = (props) => {
    const totalNumber = Math.ceil(props.totalPage / props.showPerPage);
    let buttons = [];
    if(totalNumber < props.currentPage){
        props.setCurrentPage(1);
    }
    for(let i=1; i<=totalNumber; i++){
        buttons.push(<Button no={i} key={i} currentPage={props.currentPage} setCurrentPage={props.setCurrentPage} />)
    }
  return (
    <React.Fragment>
        {
            buttons.map((button) => {                
                return button;
            })
        }
    </React.Fragment>
  )
}

export default PaginationButton