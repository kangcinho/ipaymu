import React from 'react'

const Layout = (props) => {
  return (
    <React.Fragment>
        <div className="bg-sky-700 w-screen h-14">
            <div className="flex justify-between items-center px-4 py-2">
                <div className="text-2xl font-bold text-white">iPaymu</div>
                <ul className="flex space-x-4 justify-evenly items-center">
                    <li className="font-semibold text-white cursor-pointer hover:border-b-2 border-b-2">Team</li>
                    <li className="font-semibold text-white cursor-pointer hover:border-b-2">Pembayaran</li>
                </ul>
            </div>
        </div>
        <div className="m-6">
            {props.children}
        </div>
    </React.Fragment>
  )
}

export default Layout