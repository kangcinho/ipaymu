import React from 'react'

const CardPage = (props) => {
  return (
    <div className="shadow-md ring-1 ring-slate-200 p-4 rounded-md">
        {props.children}
    </div>
  )
}

export default CardPage