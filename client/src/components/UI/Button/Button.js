import React from 'react'

const Button = (props) => {
  return (
    <div onClick={() => props.onOpenModal(true)} className="bg-blue-500 inline-block  rounded-md text-white font-semibold hover:bg-blue-700 hover:cursor-pointer active:bg-blue-500">
        <div className="flex justify-center items-center px-3 py-1">
            <div className="mr-3 text-md">+</div>
            <div className="text-sm">{props.children}</div>
        </div>
    </div>
  )
}

export default Button