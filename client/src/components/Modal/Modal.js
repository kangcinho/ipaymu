import React from 'react'

const Modal = (props) => {
  return (
    <React.Fragment>
        {/* Backdrop */}
        <div onClick={() => props.onCloseModal(false)} className="bg-gray-500 h-screen w-screen absolute z-90 top-0 bottom-0 left-0 right-0 opacity-50"></div>

        {/* Modal */}
        <div className={`bg-slate-50 z-30 opacity-100 ${props.width} ${props.height} overflow-y-auto absolute mx-auto mt-24 top-0 left-0 right-0 bottom-0 rounded-md drop-shadow-md shadow-md`}>
            <div className="flex w-full p-4 items-center justify-between bg-sky-700  font-semibold text-slate-100">
                <div className="">{props.title}</div>
                <div onClick={() => props.onCloseModal(false)} className="rounded-full text-slate-100  ring-1 ring-sky-500 mr-2 h-8 w-8 flex items-center justify-center cursor-pointer hover:bg-sky-500">
                    <span>X</span>
                </div>
            </div>
            <div className="p-4">
                {props.children}
            </div>
        </div>
    </React.Fragment>
  )
}

export default Modal