import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux";

import {post, put} from "../../../store/team/TeamAction"

const TeamFormLogic = () => {
    console.log("Team Logic Call");
    const dispatch = useDispatch();
    const team = useSelector((state) => state.team.init);
    const [uuid, setUuid] = useState("");
    const [nama, setNama] = useState("");
    const [pekerjaan, setPekerjaan] = useState("");
    const [tanggal_lahir, setTanggal_Lahir] = useState("");

    useEffect(() => {
        setUuid(team.uuid);
        setNama(team.nama);
        setPekerjaan(team.pekerjaan);
        setTanggal_Lahir(team.tanggal_lahir);
    }, [team.uuid, team.nama, team.pekerjaan, team.tanggal_lahir]);

    const onSave = () => {
        if(uuid === "" ){
            // POST
            dispatch(post(nama, pekerjaan, tanggal_lahir));
        }else{
            // PUT
            dispatch(put(uuid, nama, pekerjaan, tanggal_lahir));
        }
        setUuid("");
        setNama("");
        setPekerjaan("");
        setTanggal_Lahir("");
    }

    return {
        nama, setNama,
        pekerjaan, setPekerjaan,
        tanggal_lahir, setTanggal_Lahir,
        onSave
    }
}

export default TeamFormLogic