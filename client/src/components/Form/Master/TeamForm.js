import React from 'react'

import TeamFormLogic from './TeamFormLogic'

import Modal from '../../Modal/Modal'

const TeamForm = (props) => {
    const logic = TeamFormLogic();
    if(props.isOpen){
        return (
            <Modal title="Team Form" width="w-[50%]" height="h-[50%]" onCloseModal={props.onCloseForm}>
                <div className="grid grid-cols-2 gap-2">
                    <div className="flex flex-col">
                        <label htmlFor="nama" className="text-slate-800">Nama</label>
                        <input type="text" className="shadow-md border rounded-md w-full p-2" value={logic.nama} onChange={(e) => {logic.setNama(e.target.value)}}/>
                    </div>
                    <div className="flex flex-col">
                        <label htmlFor="pekerjaan" className="text-slate-800">Pekerjaan</label>
                        <input type="text" className="shadow-md border rounded-md w-full p-2" value={logic.pekerjaan} onChange={(e) => {logic.setPekerjaan(e.target.value)}}/>
                    </div>
                    <div className="flex flex-col">
                        <label htmlFor="tanggal_lahir" className="text-slate-800">Tanggal Lahir</label>
                        <input type="date" className="shadow-md border rounded-md w-full p-2" value={logic.tanggal_lahir} onChange={(e) => {logic.setTanggal_Lahir(e.target.value)}}/>
                    </div>
                </div>
                <div className="flex flex-col gap-2 mt-2">
                    <button className="bg-sky-500 py-2 rounded-md text-white font-semibold hover:bg-sky-600 active:bg-sky-500" onClick={() => { logic.onSave(); props.onCloseForm(false); }}>Save</button>
                    <button onClick={() => { props.onCloseForm(false)}} className="text-slate-800 rounded-md border py-2 font-semibold hover:bg-slate-600 hover:text-white active:bg-transparent active:text-slate-800">Cancel</button>
                </div>
            </Modal>
        )
    }else{
        return <React.Fragment></React.Fragment>
    }
}

export default React.memo(TeamForm);