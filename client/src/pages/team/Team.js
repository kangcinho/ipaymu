import React from 'react'
import { useSelector } from 'react-redux';

import TeamLogic from './TeamLogic'
import TeamForm from '../../components/Form/Master/TeamForm';

import Button from '../../components/UI/Button/Button';
import CardPage from '../../components/UI/Card/CardPage';

import PaginationButton from '../../components/UI/Pagination/PaginationButton';

const Team = () => {
  // console.log("TEAM PAGE");
  const logic = TeamLogic();
  const teams = useSelector((state) => state.team.teams);
  const totalPage = useSelector((state) => state.team.totalPage);
  return (
    <CardPage>
      <div className="flex justify-between">
        <div className="flex space-x-3 items-center">
          <span className="text-slate-800">Show</span>
          <select className='ring-1 ring-slate-200 rounded-md w-16' value={logic.showPage} onChange={(e) => { logic.setShowPage(e.target.value)}}>
            { 
              logic.paginations.map((page, index) => {
                return <option  key={index} className="text-slate-800" value={page} >{page}</option>
              } 
            )}
          </select>
        </div>
        <Button onOpenModal={logic.setModalOpen}>Team</Button>
      </div>
      
      <table className="w-full rounded-md border border-slate-200 mt-2 ring-1 ring-slate-200 overflow-hidden border-collapse">
        <thead>
          <tr>
            <th className='border border-slate-200 w-[20%] py-0.5 px-2'>Nama</th>
            <th className='border border-slate-200 w-[20%] py-0.5 px-2'>Pekerjaan</th>
            <th className='border border-slate-200 w-[20%] py-0.5 px-2'>Tanggal Lahir</th>
            <th className='border border-slate-200 w-[20%] py-0.5 px-2'>Ket</th>
            <th className='border border-slate-200 w-[20%] py-0.5 px-2'>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className='border border-slate-200 w-[20%] py-0.5 px-2'>
              <input type="text" className="py-1 px-2 rounded-md shadow-sm my-1 border w-full" value={logic.searchName} onChange={(e) => {logic.setSearchName(e.target.value)}}/>
            </td>
            <td className='border border-slate-200 w-[20%] py-0.5 px-2'>
              <input type="text" className="py-1 px-2 rounded-md shadow-sm my-1 border w-full" value={logic.searchPekerjaan} onChange={(e) => {logic.setSearchPekerjaan(e.target.value)}}/>
            </td>
            <td className='border border-slate-200 w-[20%] py-0.5 px-2 text-end'>
              <div className="flex justify-center items-center">
                <input type="date" className="py-1 px-2 rounded-md shadow-sm my-1 border" value={logic.searchTanggalLahirAwal} onChange={(e) => {logic.setSearchTanggalLahirAwal(e.target.value)}}/>
                <span className="font-bold text-2xl text-slate-800">-</span>
                <input type="date" className="py-1 px-2 rounded-md shadow-sm my-1 border" value={logic.searchTanggalLahirAkhir} onChange={(e) => {logic.setSearchTanggalLahirAkhir(e.target.value)}}/>
              </div>
            </td>
            <td className='border border-slate-200 w-[20%] py-0.5 px-2'></td>
            <td className='border border-slate-200 w-[20%] py-0.5 px-2 text-center'></td> 
          </tr>
          { 
            teams.map((team, index) => {
              return (
                <tr key={index}>
                  <td className='border border-slate-200 w-[20%] py-0.5 px-2'>
                    {team.nama}
                  </td>
                  <td className='border border-slate-200 w-[20%] py-0.5 px-2'>
                    {team.pekerjaan}
                  </td>
                  <td className='border border-slate-200 w-[20%] py-0.5 px-2 text-end'>
                    {team.tanggal_lahir}
                  </td>
                  <td className='border border-slate-200 w-[20%] py-0.5 px-2'>
                    {team.tgl_keterangan} - {team.week_keterangan}
                  </td>
                  <td className='border border-slate-200 w-[20%] py-0.5 px-2 text-center'>
                      <div className="flex items-center justify-center space-x-2">
                        <button className="bg-yellow-500 px-3 py-2 font-semibold text-white rounded-md hover:bg-yellow-800 hover:text-white active:bg-yellow-500" onClick={() => {logic.onGet(team.uuid) }}>Edit</button>
                        <button className="bg-pink-500 px-3 py-2 font-semibold text-white rounded-md hover:bg-pink-800 hover:text-white active:bg-pink-500" onClick={() => {logic.onDelete(team.uuid) }}>Delete</button>
                      </div>
                  </td> 
                </tr>
              )
            })
          }
        </tbody>
      </table>

      <div className="flex justify-end items-center mt-2">
        <PaginationButton totalPage={totalPage} showPerPage={logic.showPage} currentPage={logic.currentPage} setCurrentPage={logic.setCurrentPage}></PaginationButton>
      </div>

      {/* Modal Disini */}
      <TeamForm isOpen={logic.isModalOpen} onCloseForm={logic.setModalOpen}/>
    </CardPage>
  )
}

export default Team