import {useEffect, useState} from 'react'
import { useDispatch } from 'react-redux';

import {index, get, del} from "../../store/team/TeamAction"
import { TeamAction } from '../../store/team/TeamSlicer';

const paginations = [10, 20, 30]

const TeamLogic = () => {
    const dispatch = useDispatch();

    const [isModalOpen, setModalOpen] = useState(false);
    const [showPage, sShowPage] = useState(10);
    const [currentPage, sCurrentPage] = useState(1);
    const [searchName, sSearchName] = useState("");
    const [searchPekerjaan, sSearchPekerjaan] = useState("");
    const [searchTanggalLahirAwal, sSearchTanggalLahirAwal] = useState("");
    const [searchTanggalLahirAkhir, sSearchTanggalLahirAkhir] = useState("");

    const setShowPage = (val) => {
        sShowPage(val);        
    }

    const setSearchName = (val) => {
        sSearchName(val);        
    }

    const setSearchPekerjaan = (val) => {
        sSearchPekerjaan(val);        
    }
    
    const setSearchTanggalLahirAwal = (val) => {
        sSearchTanggalLahirAwal(val);        
    }
    
    const setSearchTanggalLahirAkhir = (val) => {
        sSearchTanggalLahirAkhir(val);        
    }
    
    const setCurrentPage = (val) => {
        sCurrentPage(val);        
    }

    const onGet = (uuid) => {
        dispatch(get(uuid));
        setModalOpen(true);
    }
    
    const onDelete = (uuid) => {
        dispatch(del(uuid));
    }

    useEffect(() => {
        const time = setTimeout(() => {
            dispatch(TeamAction.setOptions({
                showPage,
                currentPage,
                searchName,
                searchPekerjaan,
                searchTanggalLahirAwal,
                searchTanggalLahirAkhir
            }));
            dispatch(index())
        }, 300)
        return () => { clearTimeout(time) }
    }, [showPage, currentPage, searchName, searchPekerjaan, searchTanggalLahirAwal, searchTanggalLahirAkhir, dispatch])

    return {
        isModalOpen, setModalOpen,
        paginations,
        showPage, setShowPage,
        currentPage, setCurrentPage,
        searchName, setSearchName,
        searchPekerjaan, setSearchPekerjaan,
        searchTanggalLahirAwal, setSearchTanggalLahirAwal,
        searchTanggalLahirAkhir, setSearchTanggalLahirAkhir,
        onGet, onDelete
    }
}

export default TeamLogic