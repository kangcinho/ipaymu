import { configureStore } from '@reduxjs/toolkit'
import TeamSlicer from './team/TeamSlicer'

const store = configureStore({
    reducer: {
        team: TeamSlicer
    }
})

export default store