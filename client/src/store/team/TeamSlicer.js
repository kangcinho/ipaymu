import { createSlice } from '@reduxjs/toolkit'

const initState = {
    teams: [],
    init: {
        uuid: "",
        nama: "",
        pekerjaan: "",
        tanggal_lahir: ""
    },
    options:{
        showPage: 10,
        currentPage: 1,
        searchName: "",
        searchPekerjaan: "",
        searchTanggalLahirAwal: "",
        searchTanggalLahirAkhir: ""
    },
    totalPage: 0,
    
}

const TeamSlicer = createSlice({
    name: "team",
    initialState: initState,
    reducers:{
        post: (state, action) => {
            
        },
        put: (state, action) => {

        },
        delete: (state, action) => {

        },
        index: (state, action) => {
            const teams = action.payload.data;
            teams.map((team) => {              
                
                const date = new Date(team.tanggal_lahir)
                if(date.getDate() % 2 === 0){
                    team.tgl_keterangan = "Tanggal Genap";
                }else{
                    team.tgl_keterangan = "Tanggal Ganjil";
                }

                const firstDate = new Date(date.getFullYear(), 0, 1);

                const totalDays = Math.floor(( date - firstDate) / (24 * 60 * 60 * 1000) );
                const weekNumber = Math.ceil(totalDays/7);

                if(weekNumber % 2 === 0){
                    team.week_keterangan = "Minggu Genap";
                }else{
                    team.week_keterangan = "Minggu Ganjil";
                }

                return team;
            })
            state.teams = teams;
            state.totalPage = action.payload.totalData;
        },
        get: (state, action) => {
            state.init = action.payload;
        },
        setOptions : (state,action) => {
            state.options.showPage = action.payload.showPage;
            state.options.currentPage = action.payload.currentPage;
            state.options.searchName = action.payload.searchName;
            state.options.searchPekerjaan = action.payload.searchPekerjaan;
            state.options.searchTanggalLahirAwal = action.payload.searchTanggalLahirAwal;
            state.options.searchTanggalLahirAkhir = action.payload.searchTanggalLahirAkhir;
        }
    }
})

export const TeamAction = TeamSlicer.actions;
export default TeamSlicer.reducer;