import { TeamAction } from "./TeamSlicer"

import axios from "../../axios";

export const index = () => {
    return async(dispatch, getState) => {
       const response = await axios.post("team/index", {
            searchNama: getState().team.options.searchName ,
            searchPekerjaan : getState().team.options.searchPekerjaan,
            searchTanggalLahirAwal: getState().team.options.searchTanggalLahirAwal,
            searchTanggalLahirAkhir: getState().team.options.searchTanggalLahirAkhir,
            page: getState().team.options.currentPage,
            limit: getState().team.options.showPage
        })
        console.log("Show Pages: ", getState().team.options.showPage);

        dispatch(TeamAction.index(response.data));
    }
}

export const get = (uuid) => {
    return async(dispatch) => {
        const response = await axios.get(`team/${uuid}`);
        dispatch(TeamAction.get(response.data.data));
    }
}

export const post = (nama, pekerjaan, tanggal_lahir) => {
    return async(dispatch) => {
        await axios.post("team/", {
            nama,
            pekerjaan,
            tanggal_lahir
        })
        dispatch(index());
    }
}

export const put = (uuid, nama, pekerjaan, tanggal_lahir) => {
    return async(dispatch) => {
        await axios.put("team/", {
            uuid,
            nama,
            pekerjaan,
            tanggal_lahir
        })
        dispatch(index());
    }
}

export const del = (uuid) => {
    return async(dispatch) => {
        await axios.delete(`team/${uuid}`);
        dispatch(index());
    }
}


export const setOptions = (val) => {
    return (dispatch) => {
        dispatch(TeamAction.setOptions(val));
    }
}
