<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use DB;

use Illuminate\Support\Str;

class TeamSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i=0; $i<99; $i++){
            DB::table("teams")->insert([
                "uuid" => Str::uuid()->toString(),
                "nama" => $faker->name,
                "pekerjaan" => $faker->jobTitle,
                "tanggal_lahir" => $faker->date,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ]);
        }
    }
}
