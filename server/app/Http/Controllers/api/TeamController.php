<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Team;
use Illuminate\Support\Str;

class TeamController extends Controller
{
    public function index(Request $req){
        // page, limit, search
        $teams = Team::query();
        if($req->searchNama != null || $req->searchNama != ""){
            $teams = $teams->where("nama", "like", "%".$req->searchNama."%");
        }
        if($req->searchPekerjaan != null || $req->searchPekerjaan != ""){
            $teams = $teams->where("pekerjaan", "like", "%".$req->searchPekerjaan."%");
        }
        if($req->searchTanggalLahirAwal != null || $req->searchTanggalLahirAwal != ""){
            if($req->searchTanggalLahirAkhir != null || $req->searchTanggalLahirAkhir != ""){
                $teams = $teams->whereBetween("tanggal_lahir", [$req->searchTanggalLahirAwal, $req->searchTanggalLahirAkhir]);
            }else{
                $teams = $teams->where("tanggal_lahir", ">=", $req->searchTanggalLahirAwal);
            }
        }
        
        $totalData = clone $teams;
        $teams = $teams->orderBy("created_at", "desc");
        $teams = $teams->skip(($req->page - 1) * $req->limit)->take($req->limit);
        $teams = $teams->get();
        
        $totalData = $totalData->get()->count();

        return response()->json([
            "status" => 200,
            "data" => $teams,
            "totalData" => $totalData,
            "msg" => ""
        ]);
    }

    public function get($uuid){
        $team = Team::where("uuid", $uuid)->first();
        if($team){
            return response()->json([
                "status" => 200,
                "data" => $team,
                "msg" => ""
            ]);
        }
        return response()->json([
            "status" => 404,
            "data" => [],
            "msg" => "Data Tidak ditemukan!"
        ]);
    }

    public function post(Request $req){
        $team = new Team();
        $team->uuid = Str::uuid()->toString();
        $team->nama = $req->nama;
        $team->pekerjaan = $req->pekerjaan;
        $team->tanggal_lahir = $req->tanggal_lahir;
        $team->save();

        return response()->json([
            "status" => 200,
            "data" => $team,
            "msg" => "Data Berhasil di Simpan"
        ]);
    }

    public function put(Request $req){
        $team = Team::where("uuid", $req->uuid)->first();
        if($team){
            $team->nama = $req->nama;
            $team->pekerjaan = $req->pekerjaan;
            $team->tanggal_lahir = $req->tanggal_lahir;
            $team->update();
            return response()->json([
                "status" => 200,
                "data" => $team,
                "msg" => "Data Berhasil di Simpan"
            ]);
        }
        return response()->json([
            "status" => 404,
            "data" => [],
            "msg" => "Data Tidak ditemukan!"
        ]);
    }

    public function delete($uuid){
        $team = Team::where("uuid", $uuid)->first();
        if($team){
            $team->delete();
            return response()->json([
                "status" => 200,
                "data" => [],
                "msg" => "Data Berhasil dihapus"
            ]);
        }
        return response()->json([
            "status" => 404,
            "data" => [],
            "msg" => "Data Tidak ditemukan!"
        ]);
    }
}
