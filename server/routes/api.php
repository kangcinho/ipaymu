<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\api\TeamController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::prefix("team")->group(function(){
    Route::post("/index", [TeamController::class, "index"])->name("team.index");
    Route::get("/{uuid}", [TeamController::class, "get"])->name("team.get");
    Route::post("/", [TeamController::class, "post"])->name("team.post");
    Route::put("/", [TeamController::class, "put"])->name("team.put");
    Route::delete("/{uuid}", [TeamController::class, "delete"])->name("team.delete");
});